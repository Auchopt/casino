package by.shag.crystal.casino.repository;

import java.util.*;

public class CasinoFromDB {

    public Map<Integer, String> toMapDB() {
        Map<Integer, String> integerStringMap = new HashMap<>();

        integerStringMap.put(1, "Клецко Максим");
        integerStringMap.put(4, "Голятина Мария");
        integerStringMap.put(5, "Данилович Дмитрий");
        integerStringMap.put(6, "Грицкевич Владислав");
        integerStringMap.put(7, "Шустова Анжела");
        integerStringMap.put(3, "Литвинов Дмитрий");
        integerStringMap.put(2, "Карпович Евгений");
        integerStringMap.put(0, "Литвинчук Антон");
        return integerStringMap;
    }
}