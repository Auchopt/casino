package by.shag.crystal.casino;

import by.shag.crystal.casino.service.CasinoService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.util.ArrayList;
import java.util.List;

public class HelloController {

    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        CasinoService cs = new CasinoService();
        List<String> list = new ArrayList<>();
        for (String s : cs.toRand()) {
            list.add(s);
        }

        welcomeText.setText("                 ♛♛♛ \n           " + list.get(0) +
                "\n                   ♦\n           " + list.get(1)
                + "\n           " + list.get(2));
    }
}