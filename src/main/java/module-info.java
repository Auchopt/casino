module by.shag.crystal.casino {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.media;


    opens by.shag.crystal.casino to javafx.fxml;
    exports by.shag.crystal.casino;
}